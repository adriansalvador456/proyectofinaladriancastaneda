// Import the functions you need from the SDKs you need
import { initializeApp } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-app.js";
import { getDownloadURL, ref, getStorage } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-storage.js";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries
// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyBQw7A0s22GvSllOCbJQIa3dGAjYN2R9v4",
  authDomain: "proyectofinaladriancastaneda.firebaseapp.com",
  projectId: "proyectofinaladriancastaneda",
  storageBucket: "proyectofinaladriancastaneda.appspot.com",
  messagingSenderId: "410213263082",
  appId: "1:410213263082:web:72e9c9d49906293bbe9993"
};
// Initialize Firebase
const app = initializeApp(firebaseConfig);
const storage = getStorage(app);
// Rutas de las imágenes en Firebase Storage
const rutasImagenes = [
  'Imagen/MarioWonder.jpeg',
  'Imagen/SuperSmashBrosUltimate.jpeg',
  'Imagen/CallOfDuty.jpg',
  'Imagen/Starfield.jpeg'
  // Agrega aquí las rutas de las demás imágenes
];
// Obtener y mostrar las imágenes
const imagen1Ref = ref(storage, 'Imagen/MarioWonder.jpeg');
const imagen2Ref = ref(storage, 'Imagen/SuperSmashBrosUltimate.jpeg');
const imagen3Ref = ref(storage, 'Imagen/CallOfDuty.jpg');
const imagen4Ref = ref(storage, 'Imagen/Starfield.jpeg');
const fondo = ref(storage,'Imagen/fondo.png');
getDownloadURL(imagen1Ref)
  .then((url1) => {
    const imagen1 = document.querySelector('.Imagen-1 img');
    imagen1.src = url1;
  })
  .catch((error) => {
    console.error(`Error al obtener la imagen 1: ${error}`);
  });
getDownloadURL(imagen2Ref)
  .then((url2) => {
  const imagen2 = document.querySelector('.Imagen-2 img');
  imagen2.src = url2;
})
.catch((error) => {
  console.error(`Error al obtener la imagen 2: ${error}`);
});
getDownloadURL(imagen3Ref)
  .then((url3) => {
  const imagen3 = document.querySelector('.Imagen-3 img');
  imagen3.src = url3;
})
.catch((error) => {
  console.error(`Error al obtener la imagen 3: ${error}`);
});
getDownloadURL(imagen4Ref)
  .then((url4) => {
  const imagen4 = document.querySelector('.Imagen-4 img');
  imagen4.src = url4;
})
.catch((error) => {
  console.error(`Error al obtener la imagen 4: ${error}`);
});