// Import the functions you need from the SDKs you need
import { initializeApp } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-app.js";
import { getAuth } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-auth.js"
import { getDownloadURL, ref, getStorage } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-storage.js";
import {getDatabase, ref, set} from "https://www.gstatic.com/firebasejs/10.5.2/firebase-database.js"
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyBgLBbjIMAayGTBSMPkOaKXAAmYmzS2vlM",
  authDomain: "proyectofinaljosevaldez.firebaseapp.com",
  projectId: "proyectofinaljosevaldez",
  storageBucket: "proyectofinaljosevaldez.appspot.com",
  messagingSenderId: "638606694870",
  appId: "1:638606694870:web:a218058d2c306ea220ec77",
};

// Initialize Firebase
export const app = initializeApp(firebaseConfig);
export const auth = getAuth(app)
export const database = getDatabase(app);

const rutasImagenes = [
    'Imagen/MarioWonder.jpeg',
    'Imagen/SuperSmashBrosUltimate.jpeg',
    'Imagen/CallOfDuty.jpg',
    'Imagen/Starfield.jpeg'
    // Agrega aquí las rutas de las demás imágenes
  ];
// Obtener y mostrar las imágenes
const imagen1Ref = ref(storage, 'Imagen/MarioWonder.jpeg');
const imagen2Ref = ref(storage, 'Imagen/SuperSmashBrosUltimate.jpeg');
const imagen3Ref = ref(storage, 'Imagen/CallOfDuty.jpg');
const imagen4Ref = ref(storage, 'Imagen/Starfield.jpeg');

getDownloadURL(imagen1Ref)
  .then((url1) => {
    const imagen1 = document.querySelector('.Imagen-1 img');
    imagen1.src = url1;
  })
  .catch((error) => {
    console.error(`Error al obtener la imagen 1: ${error}`);
  });
getDownloadURL(imagen2Ref)
  .then((url2) => {
  const imagen2 = document.querySelector('.Imagen-2 img');
  imagen2.src = url2;
})
.catch((error) => {
  console.error(`Error al obtener la imagen 2: ${error}`);
});
getDownloadURL(imagen3Ref)
  .then((url3) => {
  const imagen3 = document.querySelector('.Imagen-3 img');
  imagen3.src = url3;
})
.catch((error) => {
  console.error(`Error al obtener la imagen 3: ${error}`);
});
getDownloadURL(imagen4Ref)
  .then((url4) => {
  const imagen4 = document.querySelector('.Imagen-4 img');
  imagen4.src = url4;
})
.catch((error) => {
  console.error(`Error al obtener la imagen 4: ${error}`);
});

///---Database---------///
var usuario = document.getElementById("username");
var ciudad = document.getElementById("Ciudad");
var estado = document.getElementById("Estado");
var calle = document.getElementById("Calle");
var numcasa = document.getElementById("NumeroCasa");
var colonia = document.getElementById("Colonia");
var referencia = document.getElementById("Referencia");
var numtarjeta = document.getElementById("Tarjeta");
var fecha = document.getElementById("Fecha");
var cvv = document.getElementById("CVV");

var Comprarbtn = document.getElementById("Insertar");

function InsertData(){
  set(ref(database, "Juego/"+ cvv.value),{
    username: usuario.value,
    Ciudad: ciudad.value,
    Estado: estado.value,
    Calle: calle.value,
    NumeroCasa: numcasa.value,
    Colonia: colonia.value,
    Referencia: referencia.value,
    Tarjeta: numtarjeta.value,
    Fecha: fecha.value,
    CVV: cvv.value
  })
  .then(()=>{
    alert("La compra se ha realizado");
    window.location.href = "/html/compraRealizada.html"
  })
  .catch((error)=>{
    alert("unsucceful, error"+error);
  });
}

Comprarbtn.addEventListener('click',InsertData);

