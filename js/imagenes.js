// Import the functions you need from the SDKs you need
import { initializeApp } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-app.js";
import { getStorage, ref, getDownloadURL } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-storage.js";
// TODO: Add SDKs for Firebase products that you want to use
import { getAuth } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-auth.js"
// https://firebase.google.com/docs/web/setup#available-libraries
// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyBQw7A0s22GvSllOCbJQIa3dGAjYN2R9v4",
  authDomain: "proyectofinaladriancastaneda.firebaseapp.com",
  projectId: "proyectofinaladriancastaneda",
  storageBucket: "proyectofinaladriancastaneda.appspot.com",
  messagingSenderId: "410213263082",
  appId: "1:410213263082:web:72e9c9d49906293bbe9993"
};
// Initialize Firebase
export const app = initializeApp(firebaseConfig);
export const storage = getStorage(app);
export const auth = getAuth(app);

// Rutas de las imágenes en Firebase Storage
const images = [
  'Imagenes-Menu/CallOfDutyMenu.jpeg',
  'Imagenes-Menu/MarioRPGMenu.jpeg',
  'Imagenes-Menu/MarioWonderMenu.jpeg',
  'Imagenes-Menu/MenuSuperSmashBros.jpeg',
  'Imagenes-Menu/SonicSuperstarsMenu.jpeg',
];

const imagen1Ref = ref(storage, 'Imagenes-Menu/MarioWonderMenu.jpeg');
const imagen2Ref = ref(storage, 'Imagenes-Menu/MenuSuperSmashBros.jpeg');
const imagen3Ref = ref(storage, 'Imagenes-Menu/CallOfDutyMenu.jpeg');
const imagen4Ref = ref(storage, 'Imagenes-Menu/MarioRPGMenu.jpeg');
const imagen5Ref = ref(storage, 'Imagenes-Menu/SonicSuperstarsMenu.jpeg');

getDownloadURL(imagen1Ref)
  .then((url1) => {
    const imagen1 = document.querySelector('.Imagen-1 img');
    imagen1.src = url1;
  })
  .catch((error) => {
    console.error(`Error al obtener la imagen 1: ${error}`);
  });

getDownloadURL(imagen2Ref)
  .then((url2) => {
    const imagen2 = document.querySelector('.Imagen-2 img');
    imagen2.src = url2;
  })
  .catch((error) => {
    console.error(`Error al obtener la imagen 2: ${error}`);
  });

getDownloadURL(imagen3Ref)
  .then((url3) => {
    const imagen3 = document.querySelector('.Imagen-3 img');
    imagen3.src = url3;
  })
  .catch((error) => {
    console.error(`Error al obtener la imagen 3: ${error}`);
  });

getDownloadURL(imagen4Ref)
  .then((url4) => {
    const imagen4 = document.querySelector('.Imagen-4 img');
    imagen4.src = url4;
  })
  .catch((error) => {
    console.error(`Error al obtener la imagen 4: ${error}`);
  });

getDownloadURL(imagen5Ref)
  .then((url5) => {
    const imagen5 = document.querySelector('.Imagen-5 img');
    imagen5.src = url5;
  })
  .catch((error) => {
    console.error(`Error al obtener la imagen 5: ${error}`);
  });
  