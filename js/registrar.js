import { createUserWithEmailAndPassword } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-auth.js"
import { auth } from '../js/imagenes.js';


const signupForm = document.querySelector('#signup-form')

signupForm.addEventListener('submit', async (e) => {
   e.preventDefault()

   const email = signupForm['signup-email'].value
   const password = signupForm['signup-password'].value

   console.log(email, password)

  try {
     const userCredentials = await createUserWithEmailAndPassword(auth, email, password)
     console.log(userCredentials)

     const sigupModal = document.querySelector('#signupModal')
     const modal = bootstrap.Modal.getInstance(sigupModal)
     modal.hide()

  } catch (error) {
    console.log(error.message)
    console.log(error.code)
    
    if (error.code === 'auth/email-already-in-use'){
      alert('El correo ya esta en uso')
    } else if(error.code === 'auth/invalid-email'){
      alert('Correo Invalido')
    } else if (error.code === 'auth/weak-password'){
      alert('La contraseña es muy debil')
    } else if (error.code) {
      alert('Sucedio un error')
    }
  }
})