import { getAuth, signInWithEmailAndPassword } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-authentication.js";

const firebaseConfig = {
    apiKey: "AIzaSyBQw7A0s22GvSllOCbJQIa3dGAjYN2R9v4",
    authDomain: "proyectofinaladriancastaneda.firebaseapp.com",
    projectId: "proyectofinaladriancastaneda",
    storageBucket: "proyectofinaladriancastaneda.appspot.com",
    messagingSenderId: "410213263082",
    appId: "1:410213263082:web:72e9c9d49906293bbe9993"
  };

// Function to handle the sign-in process
function signIn() {
    // Get user input for email and password
    const email = document.getElementById("email").value;
    const password = document.getElementById("password").value;
  
    // Get authentication instance
    const auth = getAuth(firebaseapp);
  
    // Sign in with email and password
    signInWithEmailAndPassword(auth, email, password)
      .then((userCredential) => {
        // Redirect to "Inicio.html" on successful sign-in
        window.location.href = "Inicio.html";
      })
      .catch((error) => {
        // Handle authentication errors
        alert("Correo o contraseña incorrectos");
        console.error("Authentication error:", error);
      });
  }