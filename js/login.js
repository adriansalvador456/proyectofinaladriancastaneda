import { signInWithEmailAndPassword } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-auth.js"
import { auth } from '../js/imagenes.js';


const signInForm = document.querySelector('#login-form');

signInForm.addEventListener('submit', async e => {
    e.preventDefault()

    const email = signInForm['login-email'].value;
    const password = signInForm['login-password'].value;

    try{
    const credentials = await signInWithEmailAndPassword(auth, email, password)
    console.log(credentials)
    window.location.href = "/html/Inicio.html";

} catch (error) {
    if (error.code === 'auth/wrong-password') {
      alert("Wrong password")
    } else if (error.code === 'auth/user-not-found') {
      alert("User not found")
    } else {
      alert("Something went wrong")
    }
  }
});